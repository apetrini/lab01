package GitBlb;

import java.io.*;
import java.util.zip.InflaterInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GitBlobObject {

	String repopath;
	String hash;

	String type;
	String content;

	public String decompressZlib(byte[] compressedData) throws IOException {
		byte[] buffer=new byte[compressedData.length];
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		InflaterInputStream in=new InflaterInputStream(new ByteArrayInputStream(compressedData));
		for (int bytesRead=0; bytesRead != -1; bytesRead=in.read(buffer)) {
			out.write(buffer,0,bytesRead);
		}
		return new String(out.toByteArray(),"UTF-8");
	}



	public GitBlobObject( String repoPath, String hash ) {

		this.repopath = repoPath + "/objects/" + hash.substring(0,2) + "/" + hash.substring(2);
		this.hash = hash;
		this.content = null;
		this.type = null;

		//System.out.println("path:   " + this.repopath);

		Path fileLocation = Paths.get( this.repopath );
		try {
			byte[] data = Files.readAllBytes(fileLocation);
			String output = decompressZlib(data);
			content = output;
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		};

		//System.out.println("output:   " + this.content);
		type = content.substring(0,4);
		content = content.substring(3);
	}

	public String getType() {
		return type;
	}

	public String getContent() {
		return content.substring(5);
	}

}
