package GitTreeObj;

import java.io.*;
import java.util.zip.InflaterInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import GitObj.GitObject;

public class GitTreeObject extends GitObject {
	//assertThat(treeObject.getEntryPaths()).containsExactlyInAnyOrder("file4","file2", "dir1", "file1","README.md");
	//assertThat(treeObject.getEntry("README.md")).isExactlyInstanceOf(GitBlobObject.class);
	//assertThat(treeObject.getEntry("README.md").getHash()).isEqualTo("4452771b4a695592a82313e3253f5e073e6ead8c");
	//assertThat(treeObject.getEntry("dir1")).isExactlyInstanceOf(GitTreeObject.class);
	//assertThat(treeObject.getEntry("dir1").getType()).isEqualTo("tree");

	String hash;
	String path;
	String content;
	String commitPath;
	String[] parsedContent;

	public String decompressZlib(byte[] compressedData) throws IOException {
		byte[] buffer=new byte[compressedData.length];
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		InflaterInputStream in=new InflaterInputStream(new ByteArrayInputStream(compressedData));
		for (int bytesRead=0; bytesRead != -1; bytesRead=in.read(buffer)) {
			out.write(buffer,0,bytesRead);
		}
		return new String(out.toByteArray(),"UTF-8");
	}

	public GitTreeObject( String repopath, String mastertreehash ) {
		hash = mastertreehash;
		path = repopath;
		commitPath = repopath + "/objects/" + hash.substring(0,2) + "/" + hash.substring(2);

		Path fileLocation = Paths.get( commitPath );
		try {
			byte[] data = Files.readAllBytes(fileLocation);
			content = decompressZlib(data);
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		};

		System.out.println("Contenuto del tree:   " + content);
		parsedContent = content.split(" |\n");
	}

	public String[] getEntryPaths() {
		return null;
	}

	public GitObject getEntry(String entry) {
		return null;
	}

	public String getHash(){
		return hash;
	}

}
