package GitCommObj;

import java.io.*;
import java.util.zip.InflaterInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import GitObj.GitObject;


public class GitCommitObject extends GitObject {
	String hash;
	String path;
	String content;
	String commitPath;
	String[] parsedContent;

	public String decompressZlib(byte[] compressedData) throws IOException {
		byte[] buffer=new byte[compressedData.length];
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		InflaterInputStream in=new InflaterInputStream(new ByteArrayInputStream(compressedData));
		for (int bytesRead=0; bytesRead != -1; bytesRead=in.read(buffer)) {
			out.write(buffer,0,bytesRead);
		}
		return new String(out.toByteArray(),"UTF-8");
	}

	public GitCommitObject( String repopath, String masterCommitHash ) {
		hash = masterCommitHash;
		path = repopath;
		commitPath = repopath + "/objects/" + hash.substring(0,2) + "/" + hash.substring(2);

		Path fileLocation = Paths.get( commitPath );
		try {
			byte[] data = Files.readAllBytes(fileLocation);
			content = decompressZlib(data);
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		};

		//System.out.println("Contenuto del commit:   " + content);
		parsedContent = content.split(" |\n");
	}


	public String getHash() {
		return hash;
	}

	public String getTreeHash() {
		return parsedContent[2];
	}

	public String getParentHash() {
		return parsedContent[4];
	}

	public String getAuthor() {
		return parsedContent[6] + " " + parsedContent[7] + " " + parsedContent[8];
	}

}
